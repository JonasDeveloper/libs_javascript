/*!
 * BIBLIOTECA: FatiarTexto
 * VERSÃO: 1.0.0
 * DATA: 2014-04-07
 * ATUALIZAÇÕES E DOCUMENTAÇÃO: http://www.jonasdeveloper.com
 * AUTORES: Jonas Santos: jonas@jonasdeveloper.com, Samuel Magalhães: samuel.alves3005@hotmail.com
 **/
(function($){
	
	$.fn.fatiarTextos=function(options)
	{
		//VALORES PADRÕES
		var config=$.extend(
		{	
			efeitoPronto:'',
			tipoCorte:'letras',
			vertical:0,
			horizontal:0,
			tempoEfeito:0.2,			
			tempoDelay:0.2,
			opacidade:1,
			rotacao:0,
			rotacaoX:0,
			rotacaoY:0,
			escalaX:0,
			escalaY:0,
			entrarSair:'entrar',
			tipoEfeito:"Linear.easeNone",
			repetir:0,
			voltarEfeito:false
		},options);
		
		//define o metodo do corte
		qualCorte = config.tipoCorte;
		//controla o tempo do delay
		var controlaDelay = config.tempoDelay;
		
		//tipos dos cortes existentes
		var cortes =
		{			
			letras : function() {//letras			
				return this.each(function(e) {					
					var idTexto = $(this).attr('id');
					var crt = " ";//alt+255
					efetuaCorte($(this).children("br").replaceWith(crt).end(), '', 'letra', '', idTexto);										
				});
			},	
			palavras : function() { //palavras	
				return this.each(function() {
					var idTexto = $(this).attr('id');
					var crt = " ";//alt+255
					efetuaCorte($(this).children("br").replaceWith(crt).end(), ' ', 'palavra', ' ', idTexto);
				});
			},			
			linhas : function() {//linhas
				return this.each(function() {
					var idTexto = $(this).attr('id');					
					var crt = 'cortar';
					efetuaCorte($(this).children("br").replaceWith(crt).end(), crt, 'linha', '<br>', idTexto);					
				});
			}
		};
			
			
		if(qualCorte && cortes[qualCorte]){		
			return cortes[ qualCorte ].apply( this, [].slice.call( arguments, 1));
		}else if( qualCorte === 'coco' || ! qualCorte ){
			return cortes.letras.apply( this, [].slice.call( arguments, 0 ));
		}
		return this;
		
		
		//funcão responsavel por definir o modo de cortar os textos
		function efetuaCorte(txt, recebeCorte, tipoClass, ajustar, idTexto) {
			
			var qtdElementos = 0;//qtd de elementos
			var todoTexto = txt.text().split(recebeCorte), cadaElemento = '';//recebe texto completo, e o corta
			//console.log(todoTexto);
			if(tipoClass == 'palavra'){//adicionando e animando o corte de tipo 'palavras'
				if (todoTexto.length) {
					$(todoTexto).each(function(i, item){
						
						if(todoTexto[i] == ' '){											
							cadaElemento += '<div id="'+idTexto+qtdElementos+'" class="'+tipoClass+(i+1)+'" style="float:left;"> </div>'+ajustar;
							qtdElementos++;
						}
						else
						{									
							if(item == " "){//conferindo alt+255
								cadaElemento += '<br>';
								qtdElementos++;
							}
							else{
								cadaElemento += '<div id="'+idTexto+qtdElementos+'" class="'+tipoClass+(i+1)+'" style="float:left;">'+ajustar+item+'</div>';
								qtdElementos++;
							}
						}
					});	
					txt.empty().append(cadaElemento);
					if(config.efeitoPronto == "Explosao")
					{
						explosao();
					}
					else if(config.efeitoPronto == "Digitacao")
					{
						digitacao();
					}
					else
					{
						for(j = 0; j <=qtdElementos; j++)
						{
							if(config.entrarSair == 'sair')
							{
								TweenMax.to('#'+idTexto+j, config.tempoEfeito, {y:config.vertical, x:config.horizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								config.tempoDelay+=controlaDelay;
							}
							else if(config.entrarSair == 'entrar')
							{
								TweenMax.from('#'+idTexto+j, config.tempoEfeito, {y:config.vertical, x:config.horizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								config.tempoDelay+=controlaDelay;
							}
						}
					}
				}
			}else if(tipoClass == 'linha'){//adicionando e animando o corte de tipo 'linhas'
				if (todoTexto.length) {
					$(todoTexto).each(function(i, item) {
						cadaElemento += '<div id="'+idTexto+qtdElementos+'" class="'+tipoClass+(i+1)+'" style="float:left;">'+item+'</div>'+ajustar;
						qtdElementos++;			
					});	
					txt.empty().append(cadaElemento);
					if(config.efeitoPronto == "Explosao")
					{
						explosao();
					}
					else if(config.efeitoPronto == "Digitacao")
					{
						digitacao();
					}
					else
					{
						for(j = 0; j <=qtdElementos; j++)
						{
							if(config.entrarSair == 'sair')
							{
								TweenMax.to('#'+idTexto+j, config.tempoEfeito, {y:config.vertical, x:config.horizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								config.tempoDelay+=controlaDelay;
								console.log(config.horizontal);
							}
							else if(config.entrarSair == 'entrar')
							{
								TweenMax.from('#'+idTexto+j, config.tempoEfeito, {y:config.vertical, x:config.horizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								config.tempoDelay+=controlaDelay;
							}
						}
					}
				}
			}else if(tipoClass == 'letra'){//adicionando e animando o corte de tipo 'letras'
				if (todoTexto.length) {
					$(todoTexto).each(function(i, item) {
				
						if(todoTexto[i] == ' ')
						{							
							cadaElemento += '<div id="'+idTexto+qtdElementos+'" class="'+tipoClass+(i+1)+'" style="float:left;"> </div>'+ajustar;
							qtdElementos++;
						}
						else
						{
							if(item == " ")//conferindo alt+255
							{
								cadaElemento += '<br>';
								qtdElementos++;
							}
							else
							{
								cadaElemento += '<div id="'+idTexto+qtdElementos+'" class="'+tipoClass+(i+1)+'" style="float:left;">'+item+'</div>'+ajustar;
								qtdElementos++;
							}
						}
					});	
					txt.empty().append(cadaElemento);	
					if(config.efeitoPronto == "Explosao")
					{
						explosao();
					}
					else if(config.efeitoPronto == "Digitacao")
					{
						digitacao();
					}
					else
					{
						for(j = 0; j <=qtdElementos; j++)
						{
							if(config.entrarSair == 'sair')
							{
								TweenMax.to('#'+idTexto+j, config.tempoEfeito, {y:config.vertical, x:config.horizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								config.tempoDelay+=controlaDelay;
							}
							else if(config.entrarSair == 'entrar')
							{
								TweenMax.from('#'+idTexto+j, config.tempoEfeito, {y:config.vertical, x:config.horizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								config.tempoDelay+=controlaDelay;
							}
						}
					}
				}
			}
			
			//efeito pronto explosao 
			function explosao()
			{
				for(j = 0; j <=qtdElementos; j++)
				{
					if(config.entrarSair == 'sair')
					{
						TweenMax.to('#'+idTexto+j, config.tempoEfeito, {x:Math.floor(Math.random() * (650 - (-650) + 1)) + (-650), y:Math.floor(Math.random() * (250 - (-250) + 1)) + (-250), scaleX:Math.random() * 4 - 2, scaleY:Math.random() * 4 - 2, rotation:Math.random() * 360 - 180, autoAlpha:config.opacidade, delay:Math.random() * 0.5, ease:config.tipoEfeito, repeat:config.repetir, yoyo:config.voltarEfeito});
						config.tempoDelay+=controlaDelay;
					}
					else if(config.entrarSair == 'entrar')
					{
						TweenMax.from('#'+idTexto+j, config.tempoEfeito, {x:Math.floor(Math.random() * (650 - (-650) + 1)) + (-650), y:Math.floor(Math.random() * (250 - (-250) + 1)) + (-250), scaleX:Math.random() * 4 - 2, scaleY:Math.random() * 4 - 2, rotation:Math.random() * 360 - 180, autoAlpha:config.opacidade, delay:Math.random() * 0.5, ease:config.tipoEfeito, repeat:config.repetir, yoyo:config.voltarEfeito});
						config.tempoDelay+=controlaDelay;
					}
				}	
			}
			
			//efeito pronto digitacao
			function digitacao()
			{								
				
				if(config.entrarSair == 'sair')
				{
					for(j = qtdElementos; j >=0; j--)
					{
						TweenMax.to('#'+idTexto+j, 0.02, {y:config.vertical, x:config.horizontal, autoAlpha:0, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:1, scaleY:1, repeat:config.repetir, yoyo:config.voltarEfeito});
						config.tempoDelay+=controlaDelay;
					}
				}
				else if(config.entrarSair == 'entrar')
				{
					for(j = 0; j <=qtdElementos; j++)
					{
						TweenMax.from('#'+idTexto+j, 0.02, {y:config.vertical, x:config.horizontal, autoAlpha:0, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:1, scaleY:1, repeat:config.repetir, yoyo:config.voltarEfeito});
						config.tempoDelay+=controlaDelay;
					}
				}	
			}
			
		}//fim efetua corte
		
	};
})(jQuery);