/*!
 * BIBLIOTECA: FatiarImagem
 * VERSÃO: 1.0.0
 * DATA: 2014-03-26
 * ATUALIZAÇÕES E DOCUMENTAÇÃO: http://www.jonasdeveloper.com
 * AUTORES: Jonas Santos: jonas@jonasdeveloper.com, Samuel Magalhães: samuel.alves3005@hotmail.com
 **/

(function($)
{
	$.fn.fatiarImagem=function(options)
	{
		//PEGA ID DA IMAGEM A SER ANIMADA	
		var idImg = $(this).attr('id');
		//VALORES PADRÕES
		var config=$.extend(
		{
			tipoFatia:'v',
			efeitoVertical:0,
			efeitoHorizontal:0,
			quantasFatias: 5,
			tempoEfeito:0.5,
			opacidade:1,
			tempoDelay:0.02,
			rotacao:0,
			rotacaoX:0,
			rotacaoY:0,
			escalaX:0,
			escalaY:0,
			entrarSair:'sair',
			tipoEfeito:"Linear.easeNone",
			repetir:0,
			voltarEfeito:false
		},options);
		//CONTROLA O DELAY DAS ANIMAÇÕES
		var controlaDelay = config.tempoDelay;
		
		//LIMITAR EFEITO QUADRADO: MÁX = 10
		if((config.tipoFatia == 'q' && config.quantasFatias >= 10) || (config.tipoFatia == 'Q' && config.quantasFatias >= 10))
		{
			config.quantasFatias = 10;			
		}
			
		return this.each(function()
		{
			var esse=this;
			
			function fatiar()
			{
				//CONTA QUANTAS IMAGENS SERÃO ANIMADAS
				var cnt=$("[id^=blocoFatia_]").length+1;
				//CRIA DIV QUE RECEBERA AS IMAGENS CORTADAS
				$('<div id="blocoFatia_'+cnt+'"></div>').insertAfter($(esse));
				
				//CONFIGURAÇÃO PARA EFEITO HORIZONTAL
				if(config.tipoFatia == 'h' || config.tipoFatia == 'H')
				{
					var w=$(esse).width(),
					h=$(esse).height(),
					sh=(h/config.quantasFatias),
					setarCSS="position: relative; float:left; margin-right: 0px; background-image: url('"+esse.src+"'); width: "+w+"px; height: "+sh+"px;",spstyle="position: absolute; left: 0px; width: 0px; height: "+sh+"px; top: ";
				
				}
				//CONFIGURAÇÃO PARA EFEITO VERTICAL
				else if(config.tipoFatia == 'v' || config.tipoFatia == 'V')
				{
					var h=$(esse).height(),
					w=$(esse).width(),
					sw=(w/config.quantasFatias),
					setarCSS="position: relative; float:left; margin-right: 0px; background-image: url('"+esse.src+"'); width: "+sw+"px; height: "+h+"px;",spstyle="position: absolute; left: 0px; width: "+sw+"px; height: 0px; top: ";
				}
								
				//CRIANDO AS IMAGENS CORTADAS				
				for(var fatias=0; fatias < config.quantasFatias; fatias++)
				{
					
					
								
					//CONFIGURAÇÃO DO BACKGROUND NO EFEITO HORIZONTAL
					if(config.tipoFatia == 'h' || config.tipoFatia == 'H')
					{
						setarCSS+=" background-position: 0px -"+(h-((config.quantasFatias-fatias)*sh))+"px;";
					}
					//CONFIGURAÇÃO DO BACKGROUND NO EFEITO VERTICAL
					else if(config.tipoFatia == 'v' || config.tipoFatia == 'V')
					{
						setarCSS+=" background-position: -"+(w-((config.quantasFatias-fatias)*sw))+"px 100%;";
					}
					
					//ADD AS TIRAS 
					if(config.tipoFatia == 'v' || config.tipoFatia == 'V' || config.tipoFatia == 'h' || config.tipoFatia == 'H')
					{
						$('<div id="'+idImg+fatias+'" style="'+setarCSS+'"><span style="'+spstyle+'"></span></div>').appendTo($('#blocoFatia_'+cnt));					
					}
					
					//SE O RESULTADO DO IF POR PAR, A ANIMAÇÃO SERAR COM VALORES NEGATIVOS
					if(fatias%2==0)
					{
						if(config.entrarSair == 'sair')
						{
							TweenMax.to($('#'+idImg+fatias), config.tempoEfeito, {y:'-'+config.efeitoVertical, x:'-'+config.efeitoHorizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
						}
						else if(config.entrarSair == 'entrar')
						{
							TweenMax.from($('#'+idImg+fatias), config.tempoEfeito, {y:'-'+config.efeitoVertical, x:'-'+config.efeitoHorizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
						}
					}
					//SE O RESULTADO DO IF POR IMPAR, A ANIMAÇÃO SERAR COM VALORES POSITIVOS
					else
					{
						if(config.entrarSair == 'sair')
						{
							TweenMax.to($('#'+idImg+fatias), config.tempoEfeito, {y:config.efeitoVertical, x:config.efeitoHorizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
						}
						else if(config.entrarSair == 'entrar')
						{
							TweenMax.from($('#'+idImg+fatias), config.tempoEfeito, {y:config.efeitoVertical, x:config.efeitoHorizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
						}
					}
					//SETANDO O DELAY PARA CRIAR UM EFEITO BEM BACANA ;)						
					config.tempoDelay+=controlaDelay;
														
					//CONFIGURAÇÃO DO BACKGROUND NO EFEITO QUADRADO / PARA ISSO USAMOS UM FOR A MAIS PARA CRIAR AS LINHAS
					if(config.tipoFatia == 'q' || config.tipoFatia == 'Q')
					{
						//CONFIGURAÇÃO PARA EFEITO QUADRADO								
						var h=$(esse).height(),
						sh=(h/config.quantasFatias),
						w=$(esse).width(),
						sw=(w/config.quantasFatias),
						setarCSS="position: relative; float:left; margin-right: 0px; background-image: url('"+esse.src+"'); width: "+sw+"px; height: "+sh+"px;",spstyle="position: absolute; left: 0px; width: "+sw+"px; height: "+sh+"px; top: ";
						
						//PARA CONTROLAR O BACKGROUND
						var ver = -(h-((config.quantasFatias-fatias)*sh));
						var hor = 0;
					
						for(var i =0; i < config.quantasFatias; i++)
						{	
													
							//CONFIGURAÇÃO DO BACKGROUND NO EFEITO QUADRADO ZICA
							setarCSS+=" background-position: "+hor+"px "+ver+"px;";
							$('<div id="'+idImg+fatias+i+'" style="'+setarCSS+'"><span style="'+spstyle+'"></span></div>').appendTo($('#blocoFatia_'+cnt));
							hor += -w/config.quantasFatias;
																										
							//SE O RESULTADO DO IF POR PAR, A ANIMAÇÃO SERAR COM VALORES NEGATIVOS
							if(fatias%2==0)
							{
								if(config.entrarSair == 'sair')
								{
									TweenMax.to($('#'+idImg+fatias+i), config.tempoEfeito, {y:'-'+config.efeitoVertical, x:'-'+config.efeitoHorizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								}
								else if(config.entrarSair == 'entrar')
								{
									TweenMax.from($('#'+idImg+fatias+i), config.tempoEfeito, {y:'-'+config.efeitoVertical, x:'-'+config.efeitoHorizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								}
							}
							//SE O RESULTADO DO IF POR IMPAR, A ANIMAÇÃO SERAR COM VALORES POSITIVOS
							else
							{
								if(config.entrarSair == 'sair')
								{
									TweenMax.to($('#'+idImg+fatias+i), config.tempoEfeito, {y:config.efeitoVertical, x:config.efeitoHorizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								}
								else if(config.entrarSair == 'entrar')
								{
									TweenMax.from($('#'+idImg+fatias+i), config.tempoEfeito, {y:config.efeitoVertical, x:config.efeitoHorizontal, autoAlpha:config.opacidade, ease:config.tipoEfeito, delay:config.tempoDelay, rotation:config.rotacao, rotationX:config.rotacaoX, rotationY:config.rotacaoY, scale:config.escala, scaleX:config.escalaX, scaleY:config.escalaY, repeat:config.repetir, yoyo:config.voltarEfeito});
								}
							}
							//SETANDO O DELAY PARA CRIAR UM EFEITO BEM BACANA ;)						
							config.tempoDelay+=controlaDelay;					
						}
					}											
				};
				$(esse).hide();					
				}
				if(!this.complete||this.width+this.height==0)
				{
					//CARREGANDO A IMAGEM / EXECUTA ANIMAÇÃO
					var img=new Image;
					img.src=this.src;
					$(img).load(function()
					{
						fatiar()
					})}
				else
				{
					fatiar()}})}
				})
(jQuery);